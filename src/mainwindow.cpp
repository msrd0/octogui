#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDateTime>
#include <QFile>
#include <QNetworkInterface>

#ifndef OCTOGUI_COMMIT
#  warning OCTOGUI_COMMIT missing
#  define OCTOGUI_COMMIT unknown
#endif

#define QUOTE_(str) #str
#define QUOTE(str) QUOTE_(str)

QString MainWindow::octoprint_url()
{
	// TODO parse environment variable
	return "http://localhost:5000/";
}

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
	, tick(new QTimer(this))
{
	ui->setupUi(this);
	ui->octogui->setText("OctoGui " QUOTE(OCTOGUI_COMMIT));
	
	connect(tick, &QTimer::timeout, this, &MainWindow::timeout);
	tick->start(1000);
	
	auto url = octoprint_url();
	const QHostAddress& lo = QHostAddress(QHostAddress::LocalHost);
	for (auto addr : QNetworkInterface::allAddresses())
	{
		if (addr.protocol() == QAbstractSocket::IPv4Protocol && addr != lo)
		{
			url.replace("localhost", addr.toString());
			break;
		}
	}
	ui->url->setText(url);
	
	qInfo("MainWindow created");
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::timeout()
{
	auto now = QDateTime::currentDateTime();
	ui->date->setText(now.toString("ddd, dd.MM.yyyy HH:mm:ss"));
	
	QFile temp("/sys/class/thermal/thermal_zone0/temp");
	temp.open(QIODevice::ReadOnly);
	QByteArray temp_data = temp.readAll().trimmed();
	temp.close();
	auto temp_celsius = QString::number(temp_data.toULongLong() / 1000.0, 'f', 1) + " °C";
	ui->cpu_temp->setText(temp_celsius);
	
	update_cpu();
	update_mem();
}

void MainWindow::update_cpu()
{
	QFile f("/proc/stat");
	f.open(QIODevice::ReadOnly);
	QByteArray line;
	while (!(line = f.readLine()).isEmpty())
	{
		int8_t cpu = -1;
		if (line.startsWith("cpu0"))
			cpu = 0;
		else if (line.startsWith("cpu1"))
			cpu = 1;
		else if (line.startsWith("cpu2"))
			cpu = 2;
		else if (line.startsWith("cpu3"))
			cpu = 3;
		if (cpu == -1)
			continue;
		
		auto data = line.trimmed().split(' ');
		qulonglong idle=0, non_idle=0;
		for (int i = 1; i <= 8; i++)
		{
			auto d = data[i].toULongLong();
			if (i == 4 || i == 5)
				idle += d;
			else
				non_idle += d;
		}
		auto total = idle + non_idle;
		auto prev = cpu_idle[cpu] + cpu_non_idle[cpu];
		
		qulonglong total_delta=1, idle_delta=1;
		if (prev < total && cpu_idle[cpu] <= idle)
		{
			total_delta = total - prev;
			idle_delta = idle - cpu_idle[cpu];
		}
		cpu_idle[cpu] = idle;
		cpu_non_idle[cpu] = non_idle;
		
		auto percent = QString::number(100.0 * (total_delta - idle_delta) / total_delta, 'f', 0) + "%";
		switch (cpu) {
			case 0: ui->cpu1->setText(percent); break;
			case 1: ui->cpu2->setText(percent); break;
			case 2: ui->cpu3->setText(percent); break;
			case 3: ui->cpu4->setText(percent); break;
		}
	}
	f.close();
}

void MainWindow::update_mem()
{
	QFile f("/proc/meminfo");
	f.open(QIODevice::ReadOnly);
	QByteArray line;
	while (!(line = f.readLine()).isEmpty())
	{
		auto data = QString::fromUtf8(line.trimmed()).split(QRegExp("\\s+"));
		auto key = data[0];
		auto val = data[1].toULongLong();
		if (data.length() >= 3 && data[2].toLower() == "kb")
			val *= 1024;
		val /= 1048576;
		
		if (data[0].startsWith("MemTotal"))
			ui->ram->setRange(0, static_cast<int>(val));
		else if (data[0].startsWith("MemAvailable"))
			ui->ram->setValue(static_cast<int>(val));
		else if (data[0].startsWith("SwapTotal"))
			ui->swap->setRange(0, static_cast<int>(val));
		else if (data[0].startsWith("SwapFree"))
			ui->swap->setValue(static_cast<int>(val));
	}
}

void MainWindow::on_speedSlider_valueChanged(int value)
{
    ui->speedLabel->setText(QString::number(value) + " mm");
}

void MainWindow::on_toolSlider_valueChanged(int value)
{
    ui->toolLabel->setText(QString::number(value) + " mm");
}
