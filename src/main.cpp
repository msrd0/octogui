#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	
	qInfo("OctoGui starting");
	
	QIcon::setFallbackSearchPaths(QIcon::fallbackSearchPaths() << "/usr/share/icons/breeze");
	
	MainWindow w;
	w.show();
	
	return a.exec();
}
