#pragma once

#include <QMainWindow>
#include <QTimer>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
	Q_OBJECT
	
public:
	MainWindow(QWidget *parent = nullptr);
	~MainWindow();
	
	static QString octoprint_url();
	
private slots:
	void timeout();
	void on_speedSlider_valueChanged(int value);
	
	void on_toolSlider_valueChanged(int value);
	
private:
	void update_cpu();
	void update_mem();
	
private:
	Ui::MainWindow *ui;
	QTimer *tick;
	
	qulonglong cpu_idle[4];
	qulonglong cpu_non_idle[4];
};
