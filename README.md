# OctoGui

**THIS IS STILL A WORK-IN-PROGRESS**

OctoGui is a OctoPrint Frontend and Raspberry Pi System Monitor designed to run on a 5' touchscreen.

## Install

```bash
sudo apt install build-essentials cmake qt5-default libqt5waylandclient5
git clone https://gitlab.com/msrd0/octogui
mkdir octogui/build
cd octogui/build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```

## Run

On my touchscreen, the following command works (assuming you have [cage](https://github.com/Hjdskes/cage) installed):

```bash
cage -drrr -- ./octogui -platform wayland
```

## License

    OctoGui
    Copyright (C) 2019-2020 Dominic Meiser <git@msrd0.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
